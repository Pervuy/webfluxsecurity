CREATE TABLE users(
    id Serial primary key ,
    username varchar(64) NOT NULL UNIQUE ,
    password varchar(2048) NOT NULL ,
    role varchar(32) not null ,
    first_name varchar(64) NOT NULL ,
    last_name varchar(64) NOT NULL ,
    enabled BOOLEAN NOT NULL DEFAULT FALSE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP

)