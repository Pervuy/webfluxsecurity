package ua.pervuy.webfluxsecurity.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import ua.pervuy.webfluxsecurity.dto.UserDto;
import ua.pervuy.webfluxsecurity.entity.UserEntity;
@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto map(UserEntity userEntity);
    @InheritInverseConfiguration
    UserEntity map(UserDto dto);
}
