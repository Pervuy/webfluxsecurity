package ua.pervuy.webfluxsecurity.entity;

public enum UserRole {
    USER,
    ADMIN;
}
